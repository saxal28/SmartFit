import React from 'react';
import {Button, Card, Container, Content, Form, Picker, Text, Title} from "native-base";
import {IncludedWorkoutsList} from "../data/IncludedWorkouts";
import { TouchableOpacity } from 'react-native';
import {connect} from "react-redux";
import {selectWorkout} from "../redux/actions/WorkoutActions";
import bindActionCreators from "redux/src/bindActionCreators";


const WorkoutItem = ({workoutName, onValueChange}) => {
  return (
    <TouchableOpacity transparent onPress={onValueChange}>
      <Card style={{marginTop: 10, marginBottom: 10, marginLeft: 5, marginRight: 5, minHeight: 200, alignItems: 'center', justifyContent: 'center'}}>
        <Title>{workoutName}</Title>
      </Card>
    </TouchableOpacity>
  )
}


class SelectWorkoutScreen extends React.Component {

  state = { selected: null };

  onValueChange = (selected) => {
    console.log("change", selected);
    this.props.selectWorkout(selected);
  };

  render(){

    return (
      <Container>
        <Content>

          <Text style={{flex: 1, textAlign: 'center', marginTop: 10, marginBottom: 10}}>
            SELECTED WORKOUT: {this.props.selectedWorkout}
            </Text>

          {IncludedWorkoutsList.map(workout => <WorkoutItem workoutName={workout.label} onValueChange={() => this.onValueChange(workout.label)}/> )}

        </Content>
      </Container>
    )
  }
}


function mapStateToProps(state) {
  console.log("state", state);
  const { workoutReducer } = state;
  const { selectedWorkout } = workoutReducer;
  console.log('selected workout', selectedWorkout)
  return {
    selectedWorkout
  }
}

function mapDispatchToProps(dispatch) {
   return {
     selectWorkout : bindActionCreators(selectWorkout , dispatch) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectWorkoutScreen)