import HomeScreen from "./Home";
import {createStackNavigator} from "react-navigation";
import SettingsScreen from "./Settings";
import ActiveWorkoutScreen from "./ActiveWorkout";
import SelectWorkoutScreen from "./SelectWorkout";
import WorkoutSettingsScreen from "./WorkoutSettings";

const Routes = createStackNavigator({
  HomeScreen,
  ActiveWorkoutScreen,
  SettingsScreen,
  SelectWorkoutScreen,
  WorkoutSettingsScreen
});

export default Routes;