import React from 'react';
import {
  Button, Container, Text, Card, Content, Body, CardItem, Header, Left, Title,
  Icon, Right
} from "native-base";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {SCREENS} from "../ennums/ScreenEnum";
import {TouchableOpacity} from "react-native";


class HomeScreen extends React.Component {

  toSelectWorkout = () => {
    this.props.navigation.push(SCREENS.SELECT_WORKOUT);
  };

  toWorkoutSettings = () => {
    this.props.navigation.push(SCREENS.WORKOUT_SETTINGS);
  };

  toActiveWorkout = () => {
    this.props.navigation.push(SCREENS.ACTIVE_WORKOUT);
  };


  render(){

    return (
      <Container>

        <Content>

          <Card style={{marginTop: 20, marginBottom: 20, marginLeft: 10, marginRight: 10}}>
            <CardItem header>
              <Text>Data</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>Active Workout: {this.props.selectedWorkout}</Text>
              </Body>
            </CardItem>
          </Card>

          <Card style={{marginTop: 20, marginBottom: 20, marginLeft: 10, marginRight: 10}}>
            <CardItem header>
              <Text>Screens</Text>
            </CardItem>
            <CardItem>
              <Body>

              <Button full onPress={this.toSelectWorkout} style={{marginTop: 5, marginBottom: 5}}>
                <Text>Select Workout</Text>
              </Button>

              <Button full onPress={this.toActiveWorkout} style={{marginTop: 5, marginBottom: 5}}>
                <Text>Active Workout</Text>
              </Button>

                <Button full onPress={this.toWorkoutSettings} style={{marginTop: 5, marginBottom: 5}}>
                  <Text>Workout Settings</Text>
                </Button>

              </Body>
            </CardItem>
          </Card>

        </Content>

      </Container>
    )
  }
}

HomeScreen.navigationOptions = ({ navigation }) => ({
  header:
    <Header>

      <Left></Left>

      <Body>
      <Title>Home</Title>
      </Body>

      <Right>
        <Button transparent>
          <Icon
            onPress={() => navigation.push(SCREENS.SETTINGS)}
            name='settings' />
        </Button>
      </Right>

    </Header>

});

function mapStateToProps(state) {
  console.log("state", state);
  const { workoutReducer } = state;
  return { ...workoutReducer }
}

function mapDispatchToProps(dispatch) {
  // return { actions: bindActionCreators(actionCreators, dispatch) }
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen)