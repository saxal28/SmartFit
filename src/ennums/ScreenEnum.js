export const SCREENS =  {
  HOME: 'HomeScreen',
  ACTIVE_WORKOUT: 'ActiveWorkoutScreen',
  WORKOUT_SETTINGS: 'WorkoutSettingsScreen',
  SELECT_WORKOUT: 'SelectWorkoutScreen',
  SETTINGS: 'SettingsScreen'
};