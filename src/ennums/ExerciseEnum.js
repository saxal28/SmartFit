export const EXERCISES = {
  BENCH_PRESS: 'Bench Press',
  SQUAT: 'Squat',
  DEADLIFT: 'Deadlift',
  OVERHEAD_PRESS: "Overhead Press",
};